# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines for those models you wish to give write DB access
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class Categories(models.Model):
	name = models.CharField(unique=True, max_length=400, blank=True)
	idcategory = models.IntegerField(primary_key=True)
	def __unicode__(self):
		if self.name is None :
			return ''
		else:
			return self.name
	def natural_key(self):
		return self.name
	class Meta:
		managed = False
		db_table = 'categories'

class ContractTypes(models.Model):
	idcontract_type = models.IntegerField(primary_key=True)
	name = models.CharField(unique=True, max_length=400, blank=True)
	def natural_key(self):
		return self.name
	def __unicode__(self):
		if self.name is None :
			return ''
		else:
			return self.name
	class Meta:
		managed = False
		db_table = 'contract_types'

class Countries(models.Model):
	idcountry = models.IntegerField(primary_key=True)
	name = models.CharField(unique=True, max_length=400, blank=True)
	def __unicode__(self):
		if self.name is None :
			return ''
		else:
			return self.name
	def natural_key(self):
		return self.name
	class Meta:
		managed = False
		db_table = 'countries'

class Empleos(models.Model):
	link = models.CharField(max_length=200, blank=True)
	titulo = models.CharField(max_length=200, blank=True)
	idempleo = models.IntegerField(primary_key=True)
	idempleo_fuente = models.CharField(max_length=400, blank=True)
	actualizado = models.NullBooleanField()
	data_publication = models.DateField(blank=True, null=True)
	municipio = models.CharField(max_length=200, blank=True)
	vacantes = models.IntegerField(blank=True, null=True)
	descripcion = models.CharField(max_length=400, blank=True)
	experiencia_minima = models.CharField(max_length=400, blank=True)
	requisitos_minimos = models.CharField(max_length=400, blank=True)
	requisitos_deseados = models.CharField(max_length=400, blank=True)
	salario = models.CharField(max_length=400, blank=True)
	duracion = models.CharField(max_length=400, blank=True)
	horario = models.CharField(max_length=400, blank=True)
	registro_bbdd_actualizado = models.DateTimeField(blank=True, null=True)
	identerprise = models.ForeignKey('Enterprises',db_column='identerprise', null=True)
	registro_bbdd_creado = models.DateTimeField(blank=True, null=True)
	idsource = models.ForeignKey('Sources',db_column='idsource', null=True)
	idcountry = models.ForeignKey('Countries',db_column='idcountry', null=True)
	idprovince = models.ForeignKey('Provinces',db_column='idprovince', null=True)
	idminimun_studies = models.ForeignKey('MinimunStudies',db_column='idminimun_studies', null=True)
	idcategory = models.ForeignKey('Categories',db_column='idcategory', null=True)
	idlevel = models.ForeignKey('Levels',db_column='idlevel', null=True)
	idworkday = models.ForeignKey('Workdays',db_column='idworkday', null=True)
	idcontract_type = models.ForeignKey('ContractTypes',db_column='idcontract_type', null=True)
	coordinates = models.TextField(blank=True) 
	salario_normalizado_bruto_mes_max = models.IntegerField(blank=True, null=True)
	salario_normalizado_bruto_mes_min = models.IntegerField(blank=True, null=True)
	def __unicode__(self):
		return self.titulo
	def getCoordinates(self):
		if self.coordinates is None:
			return ""
		else:
			return self.coordinates.replace ("(", "").replace (")", "") 
	def natural_key(self):
		return self.name
	class Meta:
		managed = False
		db_table = 'empleos'

class Enterprises(models.Model):
	identerprise = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=400, blank=True)
	url = models.CharField(max_length=400, blank=True)
	n_workers = models.CharField(max_length=400, blank=True)
	headquarter = models.CharField(max_length=400, blank=True)
	description = models.CharField(max_length=400, blank=True)
	long = models.CharField(max_length=400, blank=True)
	lat = models.CharField(max_length=400, blank=True)
	def __unicode__(self):
		if self.name is None :
			return ''
		else:
			return self.name
	def natural_key(self):
		return self.name
	class Meta:
		managed = False
		db_table = 'enterprises'

class Levels(models.Model):
	idlevel = models.IntegerField(primary_key=True)
	name = models.CharField(unique=True, max_length=400, blank=True)
	def __unicode__(self):
		if self.name is None :
			return ''
		else:
			return self.name
	def natural_key(self):
		return self.name
	class Meta:
		managed = False
		db_table = 'levels'

class MinimunStudies(models.Model):
	idminimun_studies = models.IntegerField(primary_key=True)
	name = models.CharField(unique=True, max_length=400, blank=True)
	def __unicode__(self):
		if self.name is None :
			return ''
		else:
			return self.name
	def natural_key(self):
		return self.name
	class Meta:
		managed = False
		db_table = 'minimun_studies'

class Provinces(models.Model):
	idprovince = models.IntegerField(primary_key=True)
	idcountry = models.ForeignKey(Countries, db_column='idcountry')
	name = models.CharField(unique=True, max_length=400, blank=True)
	ine_code = models.IntegerField(blank=True, null=True)
	ine_name = models.CharField(max_length=400, blank=True)
	def __unicode__(self):
		if self.name is None :
			return ''
		else:
			return self.name
	def natural_key(self):
		return self.name
	class Meta:
		managed = False
		db_table = 'provinces'
	
		
class Municipies(models.Model):
	idmunicipie = models.IntegerField(primary_key=True)
	iprovince = models.ForeignKey('Provinces', db_column='idprovince')
	idcountry = models.ForeignKey(Countries, db_column='idcountry')
	name = models.CharField(max_length=400, blank=True)
	ine_name = models.CharField(max_length=400, blank=True)
	ine_code_province = models.IntegerField(blank=True, null=True)
	ine_code = models.IntegerField(blank=True, null=True)
	def __unicode__(self):
		if self.name is None :
			return ''
		else:
			return self.name
	def natural_key(self):
		return self.name
	class Meta:
		managed = False
		db_table = 'municipies'

class Sources(models.Model):
	idsource = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=400, blank=True)
	url = models.CharField(max_length=400, blank=True)
	def __unicode__(self):
		if self.name is None :
			return ''
		else:
			return self.name
	def natural_key(self):
		return self.name
	class Meta:
		managed = False
		db_table = 'sources'

class Workdays(models.Model):
	idworkday = models.IntegerField(primary_key=True)
	name = models.CharField(unique=True, max_length=400, blank=True)
	def __unicode__(self):
		if self.name is None :
			return ''
		else:
			return self.name
	def natural_key(self):
		return self.name
	class Meta:
		managed = False
		db_table = 'workdays'

