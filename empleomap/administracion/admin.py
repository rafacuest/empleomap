from django.contrib import admin

# Register your models here.

import models

class CategoriesAdmin(admin.ModelAdmin):
	#fields = ['idcategory', 'name']   #canviando el orden se canbia el orden de aparicin en la vista no en la lista
	fieldsets = [
		(None,			   {'fields': ['idcategory']}),
		('Hola', {'fields': ['name']}),
	]
admin.site.register(models.Categories, CategoriesAdmin)

class EmpleosAdmin(admin.ModelAdmin):
	#list_display = ('titulo', 'link')	#canviar las columnas de la lista
	list_display = ('titulo', 'link','getCoordinates')	#establecer una columna personlaizada definida en el modelo
	search_fields = ['titulo']	#anade un box de busqueda al inicio del listado
admin.site.register(models.Empleos,EmpleosAdmin)

class ContriesAdmin(admin.ModelAdmin):
	list_display = ['name']

admin.site.register(models.Countries, ContriesAdmin)

class EnterprisesAdmin(admin.ModelAdmin):
	list_display = ('name','lat','long')
admin.site.register(models.Enterprises,EnterprisesAdmin)

admin.site.register(models.ContractTypes)
admin.site.register(models.Levels)
admin.site.register(models.MinimunStudies)
admin.site.register(models.Municipies)
admin.site.register(models.Provinces )
admin.site.register(models.Sources )
admin.site.register(models.Workdays)
