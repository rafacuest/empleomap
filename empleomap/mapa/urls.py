from django.conf.urls import patterns, url

from mapa import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^empresas$', views.empresas, name='empresas'),
    #gestiona la pagina de inicio
    url(r'^jobs$', views.jobs, name='jobs'),
    #gestiona la localizacion es decir solo catalunya y el SOC
    url(r'^jobs/(?P<loca_cat>\w+)$', views.jobs, name='jobs'),
    #retorna los json de los pois
    url(r'^getJobs$', views.getJobs, name='getJobs'),
    #Obtiene la informacion de un empleo dado su idmpleo
	url(r'^getJob$', views.getJob, name='getJob'),
	#Gestiona el formulario de contacto
	url(r'^enviarContacto$', views.enviarContacto, name='enviarContacto'),
    # ex: /polls/5/vote/
    url(r'^(?P<question_id>\d+)/vote/$', views.vote, name='vote'),
    # este es el hoock que desplega la aplicacion.
    #desde la estacion de desarrollo se hace un commit en bitbucket llama a
    #http://whereisthejob.puigverd.org/mapa/deploy que realiza el poll desde produccion 
    url(r'^deploy$', views.deploy, name='deploy'),
)
from django.conf import settings

if settings.DEBUG:
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
    )
