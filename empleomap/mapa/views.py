#from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader

from administracion.models import Empleos, Enterprises, Categories, Provinces, Sources
from django.db.models import Q
from django.core import serializers
import time
from datetime import date, timedelta
from django.utils import simplejson
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

def index(request):
	from django.shortcuts import redirect
	return redirect('jobs')

def empresas(request):
	n_enterprises = Enterprises.objects.count()
	enterprises_localizadas = Enterprises.objects.filter( ~Q(lat = None) ,~Q(long = None) )[:70]
	n_enterprises_localizadas = Enterprises.objects.filter( ~Q(lat = None) , ~Q(long = None) ).count()
	template = loader.get_template('mapa/enterprises.html')
	context = RequestContext(request, {
		'n_enterprises_localizadas' : n_enterprises_localizadas,
		'enterprises_localizadas' : enterprises_localizadas,
		'n_enterprises' : n_enterprises,
	})
	return HttpResponse(template.render(context))
	
def jobs(request, loca_cat = None):
	n_enterprises =  Enterprises.objects.count()
	n_enterprises_locates = Enterprises.objects.filter( ~Q(lat = None) ,~Q(long = None) ).count()
	n_jobs = Empleos.objects.count()
	n_jobs_locates = Empleos.objects.filter( ~Q(coordinates = None) ).count()
	categories = Categories.objects.order_by('name').all()
	provinces = Provinces.objects.order_by('name').all()
	sources = Sources.objects.order_by('name').all()
	
	date_desde = date.today()#-timedelta(days=14)
	date_desde = date_desde.strftime("%Y-%m-%d")
	date_hasta = date.today()#-timedelta(days=14)
	date_hasta = date_hasta.strftime("%Y-%m-%d")
	#Filtros para parameter por eje solo catalunya
	if loca_cat == "cat":
		from django.utils import translation
		translation.activate('ca')
		loca_cat="true"
	else:
		loca_cat="false"
		
	template = loader.get_template('mapa/jobs.html')
	context = RequestContext(request, {
		'n_jobs' : n_jobs,
		'n_jobs_locates' : n_jobs_locates,
		'n_enterprises' : n_enterprises,
		'n_enterprises_locates' : n_enterprises_locates,
		'date_desde' : date_desde,
		'date_hasta' : date_hasta,
		'categories' : categories,
		'provinces' : provinces,
		'sources' : sources,
		'loca_cat' : loca_cat,
	})
	return HttpResponse(template.render(context))
	
def getJobs(request):
	try:
		date_desde = request.GET['date_desde']
		date_hasta = request.GET['date_hasta']
		categories = request.GET.getlist('categories')
		provinces = request.GET.getlist('provinces')
		sources = request.GET.getlist('sources')
		salario = request.GET.getlist('salario')
		
		jobs_locates = Empleos.objects.filter( ~Q(coordinates = None) )
		
		if date_desde is not None:
			jobs_locates = jobs_locates.filter( data_publication__gte=date_desde )
		if date_hasta is not None:
			jobs_locates = jobs_locates.filter( data_publication__lte=date_hasta )
		if categories[0] is not u"" :
			jobs_locates = jobs_locates.filter( idcategory__in=categories )
		if provinces[0] is not u"" :
			jobs_locates = jobs_locates.filter( idprovince__in=provinces )
		if sources[0] is not u"" :
			jobs_locates = jobs_locates.filter( idsource__in=sources )
		if salario[0] is not u"" :
			print salario[0]
			aaa = salario[0].split(',')
			print aaa
			jobs_locates = jobs_locates.filter( salario_normalizado_bruto_mes_min__gte=aaa[0] )
			jobs_locates = jobs_locates.filter( salario_normalizado_bruto_mes_max__lte=aaa[1] )
			
			
		jobs_locates_json = serializers.serialize('json', jobs_locates,fields=('titulo','coordinates') )
		jobs_locates_list = simplejson.loads( jobs_locates_json )
		
		#para reducir el json de los jobs puesto que envia mucha informacion duplicada
		for index in range(len(jobs_locates_list)):
			del jobs_locates_list[index]["model"]
			jobs_locates_list[index]["f"] = jobs_locates_list[index].pop("fields")
			jobs_locates_list[index]["f"]["t"] = jobs_locates_list[index]["f"].pop("titulo")
			jobs_locates_list[index]["f"]["c"] = jobs_locates_list[index]["f"].pop("coordinates")
			
		last_sql = str(jobs_locates.query)
		n_jobs_locates = jobs_locates.count()
		
		json_data = simplejson.dumps( {
			'jobs_locates':jobs_locates_list,
			'n_jobs_locates':n_jobs_locates,
			#'last_sql' : last_sql
		})
		
	except KeyError:
		return HttpResponse('Ersssssror') # incorrect post
		
	return HttpResponse(json_data, mimetype='application/json')

def getJob(request):
	try:
		idempleo = request.GET['idempleo']
		job = Empleos.objects.filter( ~Q(coordinates = None) )
		
		if idempleo is not None:
			job = job.select_related().filter( idempleo=idempleo )
			
		enterprise_url = job[0].identerprise.url
		
		job_json = serializers.serialize('json', job ,indent=2, use_natural_keys=True)
		job = simplejson.loads( job_json )
		job[0]["fields"]["enterprise_url"] = enterprise_url
		
		json_data = simplejson.dumps( {
			'job':job,
		})
		
	except KeyError:
		return HttpResponse('Error obteniendo el empleo') # incorrect post
		
	return HttpResponse(json_data, mimetype='application/json')

#la chorrada esta  csrf_exempt es para habilitar las peticiones post sin que tenga en cuenta una cookie o algo parecido que se envia en los formularios
@csrf_exempt
def deploy(request, cat):
	import os
	#response = os.system('git reset --hard HEAD')
	#response = os.system('git pull empleomap master')
	#http://stackoverflow.com/questions/9589814/git-force-a-pull-to-overwrite-everything-on-every-pull
	response = os.system('git fetch empleomap master')
	response = os.system('git reset --hard FETCH_HEAD')
	response = os.system('git clean -df')
	import smtplib
	import time
	
	print (time.strftime("%H:%M:%S"))
	ahora = time.strftime("%H:%M:%S")
	
	SMTP_SERVER = 'smtp.gmail.com'
	SMTP_PORT = 587
	
	sender = 'rafa@puigverd.org'
	password = 'requetequete'
	recipient = 'rafa@puigverd.org'
	subject = 'Prueon'
	body = "Sincronizacion bbdd"+ahora
	
	body = "" + body + ""
	
	headers = ["From: " + sender,
			"Subject: " + subject,
			"To: " + recipient,
			"MIME-Version: 1.0",
			"Content-Type: text/html"]
	headers = "\r\n".join(headers)
	
	session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
	
	session.ehlo()
	session.starttls()
	session.ehlo
	session.login(sender, password)
	
	#session.sendmail(sender, recipient, headers + "\r\n\r\n" + body)
	session.quit()

	return HttpResponse( response )
@csrf_exempt
def enviarContacto(request):
	import smtplib
	nombre = request.GET['nombre']
	mail = request.GET['mail']
	texto = request.GET['texto']
	SMTP_SERVER = 'smtp.gmail.com'
	SMTP_PORT = 587
	
	sender = 'rafa@puigverd.org'
	password = 'requetequete'
	recipient = 'rafa@puigverd.org'
	subject = "Formulario de emplemap"+ nombre
	
	body = nombre+"<br />"+mail+"<br />" + texto + ""
	
	headers = ["From: " + sender,
			"Subject: " + subject,
			"To: " + recipient,
			"MIME-Version: 1.0",
			"Content-Type: text/html"]
	headers = "\r\n".join(headers)
	
	session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
	
	session.ehlo()
	session.starttls()
	session.ehlo
	session.login(sender, password)
	
	session.sendmail(sender, recipient, headers + "\r\n\r\n" + body)
	session.quit()

	return HttpResponse(texto, mimetype='application/json')
	
def vote(request, question_id):
	return HttpResponse("You're voting on question %s." % question_id)
