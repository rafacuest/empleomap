#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2, sys
from psycopg2 import IntegrityError
from connection import *
from datetime import *

class ProcesarRSS:
	
	def __init__(self):
		self.conn = None
		self.cur =None
		self.provincias = [
			["Barcelona" ,"9"],
			["Girona" ,"19"],
			["Lleida" ,"31"],
			["Tarragona" ,"45"],
			]
	def insert_empleo(self, link, titulo, idempleo_fuente, fuente):
		try:
			self.cur.execute("SELECT * FROM empleos WHERE link = %s", (link,))
			item = self.cur.fetchone()
			if( item != None ):
				return False	
		except:
			print "Error en la comprobación de si existe ja ese idempleo_fuente ", sys.exc_info()
			exit(0)
			
		try:
			self.cur.execute("INSERT INTO empleos (link, titulo, idempleo_fuente,idsource) VALUES (%s, %s, %s, %s)", (link, titulo, idempleo_fuente, fuente))
			self.conn.commit()
		except IntegrityError:
			print "Error de integridad ", sys.exc_info()[0]
		except:
			print "I cant INSERT"
			print self.cur.query

			sys.stdout.flush()
			exit(0)
		
	def print_empleos(self):
		try:
			self.cur.execute("""SELECT * from empleos""")
		except:
			print "I cant SELECT"
			exit(0)
		rows = self.cur.fetchall()
		print "\nRows: \n"
		for row in rows:
			print "   ", row[1]
		
	def close_database(self):	
		self.cur.close()
		self.conn.close()
	
	def procesaEmpleos(self, codigo_provincia):
		#Make the request to the yahoo api
		import urllib2
		try:
			result = urllib2.urlopen("http://query.yahooapis.com/v1/public/yql?q=select%20*%20%0Afrom%20rss%20%0Awhere%20url%3D%22http%3A%2F%2Fwww.infojobs.net%2Ftrabajos.rss%2Fp_"+str(codigo_provincia)+"%2F%22&format=json&diagnostics=true&callback=").read()
		except:
			print "I am unable to connect to the url"
			exit(0)
			
		import json, sys
		reload(sys) # estas dos instrucciones establecen la salida estandar en UTF
		sys.setdefaultencoding('UTF8')
		data = json.loads(result)
		
		if(data['query']['results'] ==  None ):
			print "la consulta a la url devuelve valor nulo nulo"
			exit(0)
		return 	data['query']['results']['item']

class ProcesarRSSSOC(ProcesarRSS):
	def metodoHija(self):
		print("Método clase hija")
		
	def procesaEmpleos(self, codigo_provincia):
		from urllib import urlopen
		from lxml import etree
		file_content = urlopen('http://feinaactiva.gencat.cat/web/guest/home/-/news_rss/rss/'+codigo_provincia).read()
		parser = etree.XMLParser(recover=True)
		xml = etree.fromstring(file_content, parser)
		
		titles = xml.xpath('//item/title/text()')
		links = xml.xpath('//item/link/text()')
		data_publications = xml.xpath('//item/pubDate/text()')
			
		empleos = []
		for index in range(len(titles)):
			#Trasnformación de las fechas
			#<pubDate>Wed, 19 Mar 2014 15:03:08 GMT</pubDate>
			date = datetime.strptime(data_publications[index].encode('utf-8'), "%a, %d %b %Y %H:%M:%S GMT")
			empleos.append({
				"title" : titles[index].encode('utf-8'),
				"link" : links[index].encode('utf-8'),
				"data_publications" : date.strftime("%Y-%d-%d")
				})
			
		return empleos	
		
def main():
	#inicializa las conecciones a la bbdd
	connection = Connection()
	connection.initDB()
	
	procesarRSSSOC = ProcesarRSSSOC()
	procesarRSSSOC.conn = connection.conn
	procesarRSSSOC.cur = connection.cur	
	procesarRSSSOC.provincias = [
		["Barcelona" ,"9"],
		["Girona" ,"19"],
		["Lleida" ,"31"],
		["Tarragona" ,"45"]
		]
	empleos = procesarRSSSOC.procesaEmpleos( 'Lleida' ) #provincia de bcn
		
	for empleo in empleos:
		link = empleo['link']
		
if  __name__ =='__main__':main()	



