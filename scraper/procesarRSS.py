#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2, sys
from psycopg2 import IntegrityError
from connection import *

class ProcesarRSS:
	
	def __init__(self):
		self.conn = None
		self.cur =None
		self.provincias = [
			["A Coruña" ,"28"],
			["Álava/Araba" ,"2"],
			["Albacete" ,"3"],
			["Alicante/Alacant" ,"4"],
			["Almería" ,"5"],
			["Asturias" ,"6"],
			["Ávila" ,"7"],
			["Badajoz" ,"8"],
			["Barcelona" ,"9"],
			["Burgos" ,"10"],
			["Cáceres" ,"11"],
			["Cádiz" ,"12"],
			["Cantabria" ,"13"],
			["Castellón/Castelló" ,"14"],
			["Ceuta" ,"15"],
			["Ciudad Real" ,"16"],
			["Córdoba" ,"17"],
			["Cuenca" ,"18"],
			["Girona" ,"19"],
			["Granada" ,"21"],
			["Guadalajara" ,"22"],
			["Guipúzcoa/Gipuzkoa" ,"23"],
			["Huelva" ,"24"],
			["Huesca" ,"25"],
			["Illes Balears" ,"26"],
			["Jaén" ,"27"],
			["La Rioja" ,"29"],
			["Las Palmas" ,"20"],
			["León" ,"30"],
			["Lleida" ,"31"],
			["Lugo" ,"32"],
			["Madrid" ,"33"],
			["Málaga" ,"34"],
			["Melilla" ,"35"],
			["Murcia" ,"36"],
			["Navarra" ,"37"],
			["Ourense" ,"38"],
			["Palencia" ,"39"],
			["Pontevedra" ,"40"],
			["Salamanca" ,"41"],
			["Santa Cruz de Tenerife" ,"46"],
			["Segovia" ,"42"],
			["Sevilla" ,"43"],
			["Soria" ,"44"],
			["Tarragona" ,"45"],
			["Teruel" ,"47"],
			["Toledo" ,"48"],
			["Valencia/València" ,"49"],
			["Valladolid" ,"50"],
			["Vizcaya/Bizkaia" ,"51"],
			["Zamora" ,"52"],
			["Zaragoza" ,"53"],
			]

	def clear_db(self):
		try:
			self.cur.execute("""
				-- Table: empleos

				DROP TABLE empleos;
				
				CREATE TABLE empleos
				(
				link character varying(200),
				titulo character varying(200),
				idempleo serial NOT NULL,
				idempleo_fuente character varying(200),
				fuente character varying(200),
				actualizado boolean DEFAULT false,
				data_publication date,
				enterprise_name character varying(200),
				municipio character varying(200),
				provincia character varying(200),
				pais character varying(200),
				categorias character varying,
				nivel character varying,
				vacantes integer,
				descripcion character varying,
				estudios_minimos character varying,
				experiencia_minima character varying,
				requisitos_minimos character varying,
				requisitos_deseados character varying,
				enterprise_url character varying,
				enterprise_n_workers character varying,
				enterprise_headquarter character varying,
				enterprise_description character varying,
				jornada_laboral character varying,
				salario character varying,
				tipo_contrato character varying,
				duracion character varying,
				horario character varying,
				CONSTRAINT idempleo PRIMARY KEY (idempleo),
				CONSTRAINT idempleo_fuente UNIQUE (idempleo_fuente)
				)
				WITH (
				OIDS=FALSE
				);
				ALTER TABLE empleos
				OWNER TO empleomap_rafacuest;

				""")
			self.conn.commit()
		except:
			print "I cant create table empleos", sys.exc_info()[0]
			exit(0)

	def insert_empleo(self, link, titulo, idempleo_fuente, fuente):
		try:
			self.cur.execute("SELECT * FROM empleos WHERE link = %s", (link,))
			item = self.cur.fetchone()
			if( item != None ):
				return False	
		except:
			print "Error en la comprobación de si existe ja ese idempleo_fuente ", sys.exc_info()
			exit(0)
			
		try:
			self.cur.execute("INSERT INTO empleos (link, titulo, idempleo_fuente,idsource) VALUES (%s, %s, %s, %s)", (link, titulo, idempleo_fuente, fuente))
			sys.stdout.flush()
			self.conn.commit()
		except IntegrityError:
			print "Error de integridad ", sys.exc_info()[0]
		except:
			print "I cant INSERT"
			exit(0)
		
	def print_empleos(self):
		try:
			self.cur.execute("""SELECT * from empleos""")
		except:
			print "I cant SELECT"
			exit(0)
		rows = self.cur.fetchall()
		print "\nRows: \n"
		for row in rows:
			print "   ", row[1]
		
	def close_database(self):	
		self.cur.close()
		self.conn.close()
	
	def procesaEmpleos(self, codigo_provincia):
		#Make the request to the yahoo api
		import urllib2
		try:
			result = urllib2.urlopen("http://query.yahooapis.com/v1/public/yql?q=select%20*%20%0Afrom%20rss%20%0Awhere%20url%3D%22http%3A%2F%2Fwww.infojobs.net%2Ftrabajos.rss%2Fp_"+str(codigo_provincia)+"%2F%22&format=json&diagnostics=true&callback=").read()
		except:
			print "I am unable to connect to the url"
			exit(0)
			
		import json, sys
		reload(sys) # estas dos instrucciones establecen la salida estandar en UTF
		sys.setdefaultencoding('UTF8')
		data = json.loads(result)
		
		if(data['query']['results'] ==  None ):
			print "la consulta a la url devuelve valor nulo nulo"
			exit(0)
		return 	data['query']['results']['item']
	
def main():
	#inicializa las conecciones a la bbdd
	connection = Connection()
	connection.initDB()
	
	procesarRSS = ProcesarRSS()
	procesarRSS.conn = connection.conn
	procesarRSS.cur = connection.cur	
	empleos = procesarRSS.procesaEmpleos( 46 ) #provincia de bcn
	for empleo in empleos:
		link = empleo['link']
		print link
if  __name__ =='__main__':main()	



