#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
reload(sys) # estas dos instrucciones establecen la salida estandar en UTF
sys.setdefaultencoding('UTF8')
class LaboralOfferSOC:
	
	def __init__(self,l_url):
		self.fields = [
			["titulo" , '//*[contains(concat(" ", normalize-space(@class), " "), " textoBlanco porcentaje_220 ")]/text()'],
			["data_publication" , u'//*[normalize-space(text())="Data publicació"]/following-sibling::*[1]/text()'],
			["enterprise_name" , '//*[normalize-space(text())="Empresa"]/following-sibling::*[2]/text()'],
			["enterprise_url" , "//no_definidoooo"],
			["enterprise_n_workers" , "//no_definidoooo"],
			["enterprise_headquarter" , "//requisitos_deseados" ],
			["enterprise_description" , "//no_definidoooo" ],
			["municipio" ,  u'//*[normalize-space(text())="Municipi"]/parent::*/text()' ],
			["provincia" , u'//*[normalize-space(text())="Província"]/parent::*/text()'],
			["pais" , "//no_definidoooo","España"],
			["categorias" , u'//*[normalize-space(text())="Categoria"]/parent::*/text()'],
			["nivel" , "//no_definidoooo" ],
			["vacantes" , u'//*[normalize-space(text())="Nombre de llocs ofertats"]/parent::*/text()' ],
			["descripcion" , u'//*[normalize-space(text())="Descripció"]/following-sibling::*[2]/text()' ],
			["estudios_minimos" , u'//*[normalize-space(text())="Nivell Formatiu"]/parent::*/text()' ],
			["experiencia_minima" , u'//*[normalize-space(text())="Detall de l\'experiència requerida"]/parent::*/text()' ],
			["requisitos_minimos" , u'//*[normalize-space(text())="Requisits"]/following-sibling::*[1]/text()' ],
			["requisitos_deseados" , "//requisitos_deseados" ],
			["oferta_no_disponible" , "//requisitos_deseados" ],
			["jornada_laboral" , u'//*[normalize-space(text())="Jornada"]/parent::*/text()' ],
			["salario" , u'//no_definidoooo' ],
			["salario_desde" , u'//*[normalize-space(text())="Salari des de"]/parent::*/text()' ],
			["salario_hasta" , u'//*[normalize-space(text())="Salari fins a"]/parent::*/text()' ],
			["tipo_contrato" , u'//*[normalize-space(text())="Relació Laboral"]/parent::*/text()' ],
			["duracion" , u'//*[normalize-space(text())="Durada del contracte"]/parent::*/text()' ],
			["horario" , u'//*[normalize-space(text())="Horari"]/parent::*/text()' ],
			]
			
		self.url = l_url
		
	def procesPageYQL(self):
		from lxml import html
		from urllib import urlopen
		from lxml import etree
		file_content = urlopen("http://"+self.url).read()
		parser = etree.XMLParser(recover=True)
		xml = etree.fromstring(file_content, parser)
		for index,field in enumerate(self.fields):
			self.fields[index].append( self.sanitize( xml.xpath( field[1] ),field[0] ) )
			
	def sanitize(self, localStr, field_name):
		#print localStr
		import re
		if localStr is not None:
			if type(localStr) is list and len(localStr) > 0:
				for index in range(len(localStr)):
					localStr[index] = re.sub(r'^[\s|\n]+', "", localStr[index])
					localStr[index] = re.sub(r'[\s|\n]+$', "", localStr[index])
				localStr = filter(None, localStr) # fastest
				try:
					localStr = localStr[0]
				except IndexError:
					localStr = ''
					
			if len(localStr) is 0:
				localStr = ""
			localStr = re.sub(r'^[\s|\n]+', "", localStr)
			localStr = re.sub(r'[\s|\n]+$', "", localStr)
			return localStr
			#print field_name+"--->"+localStr+"<---"
			
		
def main():
	myLaboralOfferSOC = LaboralOfferSOC("https://feinaactiva.gencat.cat/web/guest/home?p_p_id=jobsLocator_WAR_psocwebjobslocator&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&_jobsLocator_WAR_psocwebjobslocator_struts_action=%2Fjobslocator%2FprocessView&saveLastPath=0&jobCode=1838991")
	offer = myLaboralOfferSOC.procesPageYQL()


if __name__ == "__main__":
	main()
