#!/usr/bin/python
# -*- coding: utf-8 -*-
import re # para las expresiones regulares
import sys
reload(sys) # estas dos instrucciones establecen la salida estandar en UTF
sys.setdefaultencoding('UTF8')
class LaboralOffer:
	
	def printer(self):
		#attrs = vars(self)
		#print ''.join("%s: %s\n" % item for item in attrs.items())
		print ''.join("%s: %s\n" % item for item in self.fields.items())
			
	def procesPageYQL(self):
		import urllib2,urllib
		try:
			str_sentencias = ""
			for field in self.fields:
				str_sentencias = str_sentencias+"SELECT content FROM html WHERE url='"+self.url+"' AND ( xpath=' "+field[1]+" ' );";
			str_sentencias = "SELECT * FROM yql.query.multi WHERE queries=\""+str_sentencias+'"'
			print str_sentencias
			str_sentencias = urllib.quote(str_sentencias)
			temp = "http://query.yahooapis.com/v1/public/yql?q="+str_sentencias+"&format=json&diagnostics=true&callback="
			#print temp+"\n"
			result = urllib2.urlopen(temp).read()
		except:
			import sys
			print "Unexpected error:", sys.exc_info()[0]
			raise
			
		import json
		data = json.loads(result)
		resultados = data['query']['results']['results']
		#print resultados
		if resultados is not None:
			for index,field in enumerate(self.fields):
				#print index
				#print (resultados[index])
				#print type(resultados[index])
				
				if isinstance(resultados[index], dict):
					self.fields[index].append(''.join(self.xstr(resultados[index].values())))
				else:
					self.fields[index].append('')
	
	def xstr(self,s):#esta puñetera mierda es por si alguien pone en blanco un campo tipo el de la url de la empresa
		if s[0] is None:
			return ' '
		else:
			return s
	
	def print_offer(self,offer):
		for index,field in enumerate(self.fields):
			print self.fields[index]
			#print self.fields[index][0]+"--->"+self.fields[index][2][:20]
	
	def localRequest(self):
		try:
			page = requests.get(self.url)
			self.tree = html.fromstring(page.text)
		except:
			print "Error connectin to the URL" + self.url
			exit(0)
	def normalizarMunicipio(self,strMunicipio):
		strMunicipio = str(re.sub(',$', '', strMunicipio))
		return strMunicipio
		
	def normalizarPais(self,strPais):
		strPais = str(re.sub('\)$', '', strPais))
		strPais = str(re.sub('^\(', '', strPais))
		return strPais
		
	def normalizarFecha(self,data_publication):
		from datetime import datetime 
		from datetime import timedelta	
		import re # para las expresiones regulares
		
		fechaActual = datetime.now()
		
		match = re.search("hace (\d+)m", data_publication)
		if match:
			dias_publicada = 0
		match = re.search("hace (\d+)h", data_publication)
		if match:
			dias_publicada = 0
		match = re.search("hace (\d+)d", data_publication)
		if match:
			dias_publicada = match.group(1)
		match = re.search("hace (\d+)d", data_publication)
		if match:
			dias_publicada = match.group(1)
		match = re.search("(\d+) de (\w+)", data_publication) #Publicada el 24 de dic
		if match:
			dia = match.group(1)
			mes = match.group(2)
			if mes == "ene":
				mes = 1
			elif  mes == "feb":
				mes = 2
			elif  mes == "mar":
				mes = 3
			elif  mes == "abr":
				mes = 4
			elif  mes == "may":
				mes = 5
			elif  mes == "jun":
				mes = 6
			elif  mes == "jul":
				mes = 7
			elif  mes == "ago":
				mes = 8
			elif  mes == "sep":
				mes = 9
			elif  mes == "oct":
				mes = 10
			elif  mes == "nov": 
				mes = 11
			elif  mes == "dic":
				mes = 12 
			year = fechaActual.year
			import datetime
			fecha2 = datetime.date(int(year), int(mes), int(dia) )  # Asigna datetime específica
			diferencia = fechaActual.date() - fecha2
			dias_publicada = diferencia.days
		
		fechaPublicacion = fechaActual + timedelta(days = int(dias_publicada)*-1)
		return fechaPublicacion.strftime("%Y-%m-%d")
	
	def __init__(self,l_url):
		self.fields = [
			#["titulo" , "//*[@id=\\\\'job\\\\']/div[1]/h1"],
			#["data_publication" , "//*[@id=\\\\'prefijoFecha\\\\']/p"],
			#["enterprise_name" , "//a[@itemprop='name']"],
			#["municipio" , "//*[@id=\\\\'prefijoPoblacion\\\\']/p"],
			#["provincia" , " //*[@id=\\\\'prefijoProvincia\\\\'] "],
			#["pais" , "//*[@id=\\\\'prefijoPais\\\\']/p"],
			#["descripcion" , "//*[@id=\\\\'prefijoDescripcion1\\\\']/p" ],
			#["salario" , "//*[@id=\\\\'prefijoSalario\\\\']/p" ],
			#["horario" , "//*[contains(., \\\\'Hora\\\\')]/following-sibling::td/p" ],
			#["vacantes" , "//*[@id=\\\\'prefijoVacantes\\\\']/p" ],
			#["jornada_laboral" , "//*[@id=\\\\'prefijoJornada\\\\']/p" ],
			#["nivel" , "//*[@id=\\\\'prefijoNivelLaboral\\\\']/p" ],
			#["estudios_minimos" , "//*[@id=\\\\'prefijoEstMin\\\\']/p" ],
			#["duracion" , "//*[@id=\\\\'prefijoDuracion\\\\']/p" ],
			#["categorias" , "//*[@id=\\\\'prefijoCat\\\\']"],
			#["experiencia_minima" , "//*[@id=\\\\'prefijoExpMin\\\\']/p" ]
			#["requisitos_minimos" , "//*[@id=\\\\'prefijoReqMinimos\\\\']/p" ],
			#["requisitos_deseados" , "//*[@id=\\\\'prefijoReqDeseados\\\\']/p" ],
			
			#["enterprise_url" , "//*[@id=\\\\'id_tr_url\\\\']/td[2]/a"],
			#["enterprise_n_workers" , "//*[@id=\\\\'id_tr_numero\\\\']/td[2]/p"],
			#["enterprise_headquarter" , "//*[@id=\\\\'id_tr_sede\\\\']/td[2]/p" ],
			#["enterprise_description" , "//*[@id=\\\\'prefijoDescripcion\\\\']/p" ],
			
			#["oferta_no_disponible" , "/html/body/div/div/div[5]/h1" ],
			
			]# se tiene que poner 4 \ por que si no se escapan luego al tratar las cadenas
			
		self.url = l_url
	
	def getText(self,item):
		if item is None:
			return None
		strTemp = ""
		for index,temp in enumerate(item.contents):
			strTemp = strTemp + str(item.contents[index])
		return strTemp

	
	def procesPageLocal(self):
		
		import requests	#para las llamadas http
		from BeautifulSoup import BeautifulSoup # para escrapear
		
		r = requests.get("http://"+self.url)
		if( r.status_code == 200 ):
			#print r.content
			soup = BeautifulSoup(r.content)
		else:
			print "error fatal"
		
		data_publication = self.getText(soup.find("span", {"itemprop":"datePosted"}))
		
		data_publication = self.normalizarFecha(data_publication)
		self.fields.append(["data_publication","",data_publication])
		
		enterprise_name = self.getText(soup.find("a", {"itemprop":"name"}))
		self.fields.append(["enterprise_name","",enterprise_name])
		
		municipio = self.getText(soup.find("span", {"itemprop":"addressLocality"}))
		municipio = self.normalizarMunicipio(municipio)		
		self.fields.append(["municipio","",municipio])
		
		provincia = self.getText(soup.find("a", {"id":"prefijoProvincia"}))
		self.fields.append(["provincia","",provincia])
		 
		pais = self.normalizarPais( self.getText(soup.find("span", {"itemprop":"addressRegion"})) )
		
		self.fields.append(["pais","",pais])
		
		descripcion = self.getText(soup.find("div", {"id":"prefijoDescripcion1"}))
		self.fields.append(["descripcion","",descripcion])
		
		salario = soup.find("li", {"itemprop":"baseSalary"})
		if salario is not None:
			salario = salario.find("span", {"class":"list-default-text"}).text
		self.fields.append(["salario","",salario])
		
		horario = soup.find("h3", text="Horario")
		if horario is not None:
			horario.findNext("span").text
		self.fields.append(["horario","",horario])
		
		vacantes = self.getText(soup.find("span", {"id":"prefijoVacantes"}))
		self.fields.append(["vacantes","",vacantes])
		
		jornada_laboral = self.getText(soup.find("span", {"id":"prefijoJornada"}))
		self.fields.append(["jornada_laboral","",jornada_laboral])
		
		nivel = self.getText(soup.find("span", {"id":"prefijoNivelLaboral"}))
		self.fields.append(["nivel","",nivel])
		
		estudios_minimos = self.getText(soup.find("span", {"id":"prefijoEstMin"}))
		self.fields.append(["estudios_minimos","",estudios_minimos])
		
		duracion = soup.find("h3", text="Duración del contrato")
		if duracion is not None:
			duracion = duracion.findNext("span").text
		self.fields.append(["duracion","",duracion])
		
		categorias = self.getText( soup.find("a", {"id":"prefijoCat"}))
		self.fields.append(["categorias","",categorias])
		
		experiencia_minima = soup.find("li", {"itemprop":"experienceRequirements"})
		if experiencia_minima is not None:
			experiencia_minima = experiencia_minima.find("span").text
		self.fields.append(["experiencia_minima","",experiencia_minima])
		
		requisitos_minimos = soup.find("h3", text="Requisitos mínimos")
		if requisitos_minimos is not None:
			requisitos_minimos = requisitos_minimos.findNext("p").text
		self.fields.append(["requisitos_minimos","",requisitos_minimos])
		
		requisitos_deseados = soup.find("h3", text="Requisitos deseados")
		if requisitos_deseados is not None:
			requisitos_deseados = requisitos_deseados.findNext("p").text
		self.fields.append(["requisitos_deseados","",requisitos_deseados])
		 
		tipo_contrato = self.getText(soup.find("span", {"id":"prefijoJornada"}))
		self.fields.append(["tipo_contrato","",tipo_contrato])
		
		#para grarantizar la retrocompatibilidad
		self.fields.append(["enterprise_url","",""])
		self.fields.append(["enterprise_n_workers","",""])
		self.fields.append(["enterprise_headquarter","",""])
		self.fields.append(["enterprise_description","",""])
		
def main():
	myLaboralOffer = LaboralOffer("www.infojobs.net/santa-cruz-de-tenerife/gestor-comercial/of-i6319e9e91a42acbdde56e72b276831")
	#offer = myLaboralOffer.procesPageYQL()
	offer = myLaboralOffer.procesPageLocal()
	myLaboralOffer.print_offer(offer)

if __name__ == "__main__":
	main()
