#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
reload(sys) # estas dos instrucciones establecen la salida estandar en  UTF

from connection import *
from geopy import geocoders
import psycopg2, sys, time, urllib2

class GeorefEmpleos():
	def __init__(self):
		self.conn = None
		self.cur =None

	def getJobsWithoutCoords(self):
		try:
			self.cur.execute("""
				SELECT 	distinct(municipio), max(idempleo), max(iso_code)
				FROM 	empleos JOIN
						countries ON ( empleos.idcountry = countries.idcountry)
				WHERE
					coordinates IS NULL
					AND municipio !=''
					AND municipio  IS NOT NULL
				GROUP BY municipio
				ORDER BY municipio
				""")
			return self.cur.fetchone()		
		except:
			print "Error accediento a la tabla empleos holaaaa ", sys.exc_info()
	
	def updateLogLatNULL(self, idemplo ):
		try:
			self.cur.execute("""
				UPDATE 	empleos
				SET 	coordinates=POINT(0, 0)
				WHERE 	idempleo = %s;
				""",
				(
				idemplo,
				)
				)
			print self.cur.query
			self.conn.commit()
		except psycopg2.DatabaseError, e:
			print 'I can\'t insert in enterprises %s' % e
	
	def updateLogLat( self, idemplo, lat, long ):
		try:
			self.cur.execute("""
				UPDATE 	empleos
				SET 	coordinates=POINT(%s, %s)
				WHERE 	municipio = (SELECT municipio from  empleos where idempleo= %s)
					and (idprovince =  (SELECT idprovince from  empleos where idempleo= %s) OR idprovince IS NULL )
					and (idcountry =  (SELECT idcountry from  empleos where idempleo= %s) OR idcountry IS NULL )
				""",
				(
				lat,
				long,
				idemplo,
				idemplo,
				idemplo
				)
				)
			print self.cur.query
			self.conn.commit()
		except psycopg2.DatabaseError, e:
			print 'I can\'t insert in enterprises %s' % e
		
def main():
	from geopy import geocoders
	#geocoders = geocoders.GoogleV3( api_key="AIzaSyB6YCYaKMY4kyAw9F827oiJATnwBsqEX-o" )
	geocoders = geocoders.GoogleV3( )
	georefEmpleos = GeorefEmpleos()
	connection = Connection()
	connection.initDB()
	georefEmpleos.conn = connection.conn
	georefEmpleos.cur = connection.cur
	row = georefEmpleos.getJobsWithoutCoords()
	while row  is not None:
		print row
		try:
			from urllib import urlencode
			print row[2]
			geoResult = geocoders.geocode( row[0], components={'country':row[2]},sensor=False  )
			if geoResult is None:
				georefEmpleos.updateLogLatNULL(row[1])
				row = georefEmpleos.getJobsWithoutCoords()
				continue
			place, (lat, lng) = geoResult
			
		except ValueError as error_message:
			print( "Failed for address %s with message:\n %s" %(row[0], error_message))
			print "Error fatal"
			row = georefEmpleos.getJobsWithoutCoords()
			exit(0)
			continue	
		
		print row[1],
		print lat,
		print lng
		georefEmpleos.updateLogLat(row[1], lat, lng)
		row = georefEmpleos.getJobsWithoutCoords()
		sys.stdout.flush()
		time.sleep(20)
		
if  __name__ =='__main__':main()		


	








