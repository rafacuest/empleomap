#!/usr/bin/python
# -*- coding: utf-8 -*-
import psycopg2, sys, re
from psycopg2.extras import *
from laboraOfferSOC import *
from connection import *
from procesarRSSSOC import *
from georefEmpleos import *
import time

print "--------------"+(time.strftime("%H:%M:%S"))
inicio = (time.strftime("%H:%M:%S"))

def save_enterprise(enterprise_name,enterprise_url,enterprise_n_workers,enterprise_headquarter,enterprise_description):
	global connection
	try:
		connection.cur.execute("""
			INSERT INTO enterprises(
				name,
				url,
				n_workers,
				headquarter,
				description)
			VALUES (%s,%s,%s,%s,%s)
			RETURNING identerprise;
			""",
			(
			offer['enterprise_name'],
			offer['enterprise_url'],
			offer['enterprise_n_workers'],
			offer['enterprise_headquarter'],
			offer['enterprise_description'],
			)
			)
		identerprise = connection.cur.fetchone()[0]
		connection.conn.commit()
		return identerprise
	except psycopg2.DatabaseError, e:
		print 'I can\'t insert in enterprises %s' % e
		sys.exit(1)

def complete_empleo( offer ):
	global connection
	try:
		if offer['enterprise_name'] is None: offer['enterprise_name']=""
		offer['enterprise_name'] = offer['enterprise_name'].replace("'", "")
		connection.cur.execute("""
			SELECT identerprise
			FROM enterprises
			WHERE name = %s ;
			""",
			(
			offer['enterprise_name'],
			))
		res = connection.cur.fetchone()
		if res is None:
			identerprise = None
		else:
			identerprise = res[0]
	except:
		print "Error accediento a la tabla enterprisessssssssss!!!! ", sys.exc_info()
		exit(0)
	if identerprise is None:	# if dosen't exist the enterprise I save it
		identerprise = save_enterprise(offer['enterprise_name'], offer['enterprise_url'], offer['enterprise_n_workers'], offer['enterprise_headquarter'], offer['enterprise_description'])
	try:
		connection.cur.execute("""
			SET datestyle = "ISO, DMY";
			UPDATE empleos SET
			data_publication= %s,
			municipio= upper(%s),
			idcategory = insert_if_not_exist_els_return_primary_key('categories','idcategory','name',%s),
			idlevel = insert_if_not_exist_els_return_primary_key('levels','idlevel','name',%s),
			vacantes = %s,
			descripcion = %s,
			idminimun_studies = insert_if_not_exist_els_return_primary_key('minimun_studies','idminimun_studies','name',%s),
			experiencia_minima= %s,
			requisitos_minimos= %s,
			requisitos_deseados = %s,
			actualizado = TRUE ,
			idworkday = insert_if_not_exist_els_return_primary_key('workdays','idworkday','name',%s),
			salario=%s,
			idcontract_type = insert_if_not_exist_els_return_primary_key('contract_types','idcontract_type','name',%s),
			duracion=%s,
			horario=%s,
			identerprise = %s,
			idcountry = insert_if_not_exist_els_return_primary_key('countries','idcountry','name',%s),
			idprovince = insert_if_not_exist_els_return_primary_key('provinces','idprovince','name',%s),
			coordinates = (
				SELECT 	 coordinates
				FROM	empleos
				WHERE	municipio =  upper(%s) AND
						idprovince = insert_if_not_exist_els_return_primary_key('provinces','idprovince','name',%s) AND
						idcountry = insert_if_not_exist_els_return_primary_key('countries','idcountry','name',%s)
				LIMIT 1
				),
			salario_normalizado_bruto_mes_min=%s,
			salario_normalizado_bruto_mes_max=%s
			WHERE	link = %s
			RETURNING idempleo
			""",
			( offer['data_publication'],
			offer['municipio'],
			offer['categorias'],
			offer['nivel'],
			offer['vacantes'],
			offer['descripcion'],
			offer['estudios_minimos'],
			offer['experiencia_minima'],
			offer['requisitos_minimos'],
			offer['requisitos_deseados'],
			offer['jornada_laboral'],
			offer['salario'],
			offer['tipo_contrato'],
			offer['duracion'],
			offer['horario'],
			identerprise,
			offer['pais'],
			offer['provincia'],
			offer['municipio'],
			offer['provincia'],
			offer['pais'],
			offer["salario_normalizado_bruto_mes_min"],
			offer["salario_normalizado_bruto_mes_max"],
			offer['link'],
			))
		
		connection.conn.commit()
		#print connection.cur.query
		connection.conn.commit()
	except psycopg2.DatabaseError, e:
		print 'I can\'t update %s' % e
		sys.exit(1)

def normalizeSalary(offer):
	import re
	if offer["salario"] :
		offer["salario"] = offer["salario"].encode('utf-8')
		if offer["salario"].find("Bruto/mes") > -1:
			m_obj = re.search(ur"([\d]*\.*[\d]*).*- ([\d]*\.*[\d]*).*Bruto/mes", offer["salario"])
			if m_obj is not None:
				if m_obj.group(1):
					offer["salario_normalizado_bruto_mes_min"] = m_obj.group(1)
				if m_obj.group(2):
					offer["salario_normalizado_bruto_mes_max"] = m_obj.group(2) 	
					
		if offer["salario"].find("Bruto/hora") > -1:
			m_obj = re.search(ur"([\d]*\.*[\d]*).*- ([\d]*\.*[\d]*).*Bruto/hora", offer["salario"])
			if m_obj is not None:
				if m_obj.group(1):
					offer["salario_normalizado_bruto_mes_min"] = str(num(m_obj.group(1)) * 240)
				if m_obj.group(2):
					offer["salario_normalizado_bruto_mes_max"] = str(num(m_obj.group(2)) * 240)
					
		if offer["salario"].find(u"Bruto/a") > -1:
			print "dentro de añoo"
			m_obj = re.search(ur"([\d]*\.*[\d]*).*- ([\d]*\.*[\d]*).*Bruto/a.*", offer["salario"])
			if m_obj is not None:
				if m_obj.group(1):
					offer["salario_normalizado_bruto_mes_min"] = str(num(m_obj.group(1)) / 12)
				if m_obj.group(2):
					offer["salario_normalizado_bruto_mes_max"] = str(num(m_obj.group(2)) / 12)	
	return offer

def num(s):
	try:
		return int(s)
	except ValueError:
		return float(s)

#inicializa las conecciones a la bbdd
connection = Connection()
connection.initDB()

procesarRSSSOC = ProcesarRSSSOC()
procesarRSSSOC.conn = connection.conn
procesarRSSSOC.cur = connection.cur
for provincia in procesarRSSSOC.provincias:
	#por cada una de las provincias descarga el rss
	empleos = procesarRSSSOC.procesaEmpleos(provincia[0])
	for empleo in empleos:
		titulo = empleo['title']
		link = empleo['link'][7:]
		idempleo_fuente = re.match(r'^(.*)=([0-9]+)$', link)
		
		if idempleo_fuente:
			idempleo_fuente = idempleo_fuente.group(2)
		else:
			print "No match el ide de infojobs"
		fuente = 2
		existe = procesarRSSSOC.insert_empleo(link, titulo, idempleo_fuente, fuente)
		if existe is False:
			continue
		myLaboralOfferSOC = LaboralOfferSOC(link)
		myLaboralOfferSOC.procesPageYQL()
		print link
		offer={}
		offer['link'] = link
		for index,field in enumerate(myLaboralOfferSOC.fields):
			offer[myLaboralOfferSOC.fields[index][0]] = myLaboralOfferSOC.fields[index][2]
			if myLaboralOfferSOC.fields[index][2] == "":
				offer[myLaboralOfferSOC.fields[index][0]] = None
			
		if 	offer["salario_desde"] is not None and offer["salario_desde"] != "0" and offer["salario_hasta"] is not None and offer["salario_hasta"] != "0" :
			if num(offer["salario_desde"]) <8000 or num(offer["salario_hasta"])<10000:
				offer["salario"] = offer["salario_desde"]+" € - "+ offer["salario_hasta"] +" € Bruto/mes"
			else:
				offer["salario"] = offer["salario_desde"]+" € - "+ offer["salario_hasta"] +u" € Bruto/año"
		offer["salario_normalizado_bruto_mes_min"] = None
		offer["salario_normalizado_bruto_mes_max"] = None
		
		offer = normalizeSalary(offer)
		complete_empleo(offer)
		print "--------------"+(time.strftime("%H:%M:%S"))
		sys.stdout.flush()
		
		
		
		
		
	
