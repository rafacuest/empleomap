#!/usr/bin/python 
# -*- coding: utf-8 -*- 
import psycopg2, sys 
from psycopg2.extras import *
from laboraOffer import *
from connection import * 
  
connection = Connection() 
cur = connection.initDB() 
  
try: 
    connection.cur.execute(""" 
        SELECT  * 
        FROM    empleos 
        WHERE   actualizado != 'TRUE' 
                --AND idempleo=5430 
        ORDER BY idempleo 
        --LIMIT 1 
        """) 
    #item = connection.cur.fetchone()       
except: 
    print "Error accediento a la tabla empleos33 ", sys.exc_info() 
    exit(0) 
  
def save_enterprise(enterprise_name,enterprise_url,enterprise_n_workers,enterprise_headquarter,enterprise_description): 
    try: 
        connection.cur.execute(""" 
            INSERT INTO enterprises( 
                name, 
                url, 
                n_workers, 
                headquarter, 
                description) 
            VALUES (%s,%s,%s,%s,%s) 
            RETURNING identerprise; 
            """, 
            ( 
            offer['enterprise_name'], 
            offer['enterprise_url'], 
            offer['enterprise_n_workers'], 
            offer['enterprise_headquarter'], 
            offer['enterprise_description'], 
            ) 
            ) 
        identerprise = connection.cur.fetchone()[0] 
        connection.conn.commit() 
        return identerprise 
    except psycopg2.DatabaseError, e: 
        print 'I can\'t insert in enterprises %s' % e 
        sys.exit(1) 
  
def complete_empleo( offer): 
    try: 
        #print "SELECT identerprise FROM enterprises WHERE name = '"+offer['enterprise_name']+"'" 
        if offer['enterprise_name'] is None: offer['enterprise_name']="" 
        offer['enterprise_name'] = offer['enterprise_name'].replace("'", "") 
        connection.cur.execute(""" 
            SELECT identerprise 
            FROM enterprises 
            WHERE name = %s ; 
            """, 
            ( 
            offer['enterprise_name'], 
            )) 
        res = connection.cur.fetchone() 
        if res is None: 
            identerprise = None
        else: 
            identerprise = res[0] 
    except: 
        print "Error accediento a la tabla enterprisessssssssss!!!! ", sys.exc_info() 
        exit(0) 
          
    if identerprise is None:    # if dosen't exist the enterprise I save it 
        identerprise = save_enterprise(offer['enterprise_name'],offer['enterprise_url'],offer['enterprise_n_workers'],offer['enterprise_headquarter'],offer['enterprise_description']) 
      
    try: 
        connection.cur.execute("""
        	SET datestyle = "ISO, DMY";
        	UPDATE empleos SET 
            data_publication= %s, 
            municipio= %s, 
            idcategory = insert_if_not_exist_els_return_primary_key('categories','idcategory','name',%s), 
            idlevel = insert_if_not_exist_els_return_primary_key('levels','idlevel','name',%s), 
            vacantes = %s, 
            descripcion = %s, 
            idminimun_studies = insert_if_not_exist_els_return_primary_key('minimun_studies','idminimun_studies','name',%s), 
            experiencia_minima= %s, 
            requisitos_minimos= %s, 
            requisitos_deseados = %s, 
            actualizado = TRUE , 
            idworkday = insert_if_not_exist_els_return_primary_key('workdays','idworkday','name',%s), 
            salario=%s, 
            idcontract_type = insert_if_not_exist_els_return_primary_key('contract_types','idcontract_type','name',%s), 
            duracion=%s, 
            horario=%s, 
            identerprise = %s, 
            idcountry = insert_if_not_exist_els_return_primary_key('countries','idcountry','name',%s), 
            idprovince = insert_if_not_exist_els_return_primary_key('provinces','idprovince','name',%s) 
            WHERE   link = %s
            RETURNING idempleo;
			SET datestyle = default;""", 
            ( offer['data_publication'], 
            offer['municipio'], 
            offer['categorias'], 
            offer['nivel'], 
            offer['vacantes'], 
            offer['descripcion'], 
            offer['estudios_minimos'], 
            offer['experiencia_minima'], 
            offer['requisitos_minimos'], 
            offer['requisitos_deseados'], 
            offer['jornada_laboral'], 
            offer['salario'], 
            offer['tipo_contrato'], 
            offer['duracion'], 
            offer['horario'], 
            identerprise, 
            offer['pais'], 
            offer['provincia'], 
            offer['link'], 
            )) 
          
        connection.conn.commit() 
    except psycopg2.DatabaseError, e: 
        print 'I can\'t update %s' % e 
        sys.exit(1) 
  
for row in connection.cur.fetchall(): 
    print row['link'] 
    myLaboralOffer = LaboralOffer(row['link']) 
    myLaboralOffer.procesPageYQL() 
    #myLaboralOffer.print_offer() 
    offer={} 
    offer['link'] = row['link'] 
    print offer['link']  
    for index,field in enumerate(myLaboralOffer.fields): 
        offer[myLaboralOffer.fields[index][0]] = myLaboralOffer.fields[index][2] 
        if myLaboralOffer.fields[index][2] == "": 
            offer[myLaboralOffer.fields[index][0]] = None
    complete_empleo(offer) 
      
      
      
      
      
          
     

