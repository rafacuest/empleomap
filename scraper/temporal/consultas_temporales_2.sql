
CREATE OR REPLACE FUNCTION insert_if_not_exist_els_return_primary_key(name_table  character varying, column_id_name  character varying, column_mach_name  character varying, value_mach_name  character varying)
  RETURNS integer AS
$BODY$
DECLARE
    aaaa integer;
BEGIN
	EXECUTE '
	WITH s AS (
	    SELECT  ' || quote_ident(column_id_name) || ' 
	    FROM ' || quote_ident(name_table) || '
	    WHERE ' || quote_ident(column_mach_name) || ' =  ''''''  || quote_ident(value_mach_name) || ''''''
	), i AS (
	    INSERT INTO ' || quote_ident(name_table) || '(' || quote_ident(column_mach_name) || ')
	    SELECT '''''' || quote_ident(value_mach_name) || ''''''
	    WHERE NOT EXISTS (
		SELECT ' || quote_ident(column_mach_name) || ' 
		FROM  ' || quote_ident(name_table) || ' 
		WHERE ' || quote_ident(column_mach_name) || ' = '''''' || quote_ident(value_mach_name) || ''''''
		)
	    RETURNING ' || quote_ident(column_id_name) || ' 
	)
	SELECT ' || quote_ident(column_id_name) || ' 
	FROM i
	UNION ALL
	SELECT ' || quote_ident(column_id_name) || ' 
	FROM s;
	' ;
	

END;
$BODY$
  LANGUAGE plpgsql ;
select  insert_if_not_exist_els_return_primary_key('minimun_studies','idminimun_studies','name','sssss')