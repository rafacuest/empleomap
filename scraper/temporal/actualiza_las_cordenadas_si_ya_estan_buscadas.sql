﻿UPDATE empleos SET coordinates= (
	SELECT 	emp.coordinates
	FROM	empleos emp
	WHERE	emp.municipio = (
			SELECT  municipio 
			FROM	empleos 
			WHERE	link = 'www.infojobs.net/ferrol/medico-empresa/of-ibc4751bf9644e8b225f0ba0fc29f83'
			) AND 
		emp.idprovince = (
			SELECT  idprovince
			FROM	empleos 
			WHERE	link = 'www.infojobs.net/ferrol/medico-empresa/of-ibc4751bf9644e8b225f0ba0fc29f83'
			) AND
		emp.idcountry = (
			SELECT  idcountry 
			FROM	empleos 
			WHERE	link = 'www.infojobs.net/ferrol/medico-empresa/of-ibc4751bf9644e8b225f0ba0fc29f83'
			) AND
		emp.coordinates IS NOT NULL
	LIMIT 1
	)
WHERE	link = 'www.infojobs.net/ferrol/medico-empresa/of-ibc4751bf9644e8b225f0ba0fc29f83'


